Arduino Mega 2560
---

54 GPIO including 14 PWM and 16 Analog Inputs

  * $13.15  http://www.amazon.com/Ximico-Arduino-Cable-Atmega2560-16au-Compatible/dp/B00KG3SBE8/ref=sr_1_3?ie=UTF8&qid=1455292686&sr=8-3&keywords=arduino+mega+2560

  * $11.99  http://www.amazon.com/MEGA2560-Control-ATMEGA2560-16AU-Arduino-Compatible/dp/B00PD92EJ8/

BeagleBone Black
---

http://hackaday.com/2013/12/07/speeding-up-beaglebone-black-gpio-a-thousand-times/

  * 6 PWM
  * 16 Digital IO - 20 outputs total (if we hijack the 4 user led's)
  * 6 Analog inputs

Raspberry Pi
---

  * 17 GPIO
  * 1 SPI
  * 1 I2C
  * Audio out
  * 4 USB

Botspeak
---
  * http://botspeak.org/supported-platforms/beaglebone/

Robopanda Hacking
---
  * http://www.robocommunity.com/article/12977/robopanda-disassembled-a-look-inside-this-friendly-robotic-bear
  * https://github.com/bcarroll/robopanda/
  * http://www.robocommunity.com/article/12973/wowwee-robopandas-rfid-friend

GPIO of RoboPanda
---

GPIO Needs 43 total all are 5 volt

IO Grouping
===

  * Left Hand (1 input, 2 outputs)
  * Left Arm (1 input, 2 outputs)
  * Right Hand (1 input, 2 outputs)
  * Right Arm (1 input, 2 outputs)
  * Head Up/Down (1 input, 2 outputs)
  * Head Left/Right (1 input, 2 outputs)
  * Eyebrows Up/Down (2 inputs, 2 outputs)
  * Ears Forward/Back (2 inputs, 2 outputs)
  * Left Leg (1 input, 2 outputs)
  * Right Leg (1 input, 2 outputs)
  * IR (1 input, 1 output)
  * Accelerometer (MEMSIC MXA2500 +/- 1g dual axis accelerometer, 4 pin header 2 data + 2 power?)
  * RFID Reader (3 pin header, 1 data + 2 power?)
  * Mic (1 input)
  * Speaker (1 output)
  * Chest LED (1 output)
  * Eye LED's (2 outputs)
  * Hand LED's (2 outputs)
  * Tilt switch (3 pin header?  No clue)
  * Touch Sensor Board (Serial, No clue)

Input sensors:
===

  * Accelerometer         - Left side
  * Ball switch           - Back
  * Capacitive touch      - Hands, Feet, Back, Chest, Back of head
  * Microphone            - Chest
  * RFID reader           - Chest
  * Infra-red detector    - Left eye
  * Potentiometer         - Motor location encoder
  * Limit switches        - Eye brows and Ears

Output sensors:
===

  * LED                   - Hands, Eyes, Chest.
  * Speaker               - Chest
  * DC Motor              - Hips, Shoulders, Upper arms, Head, Ears, Eye brows
  * InfraRed emitter      - Nose

18 Inputs
===

  * Left Hand Analog
  * Left Arm Analog
  * Head Up/Down Analog
  * Ball Switch
  * RFID Reader
  * Right Arm Analog
  * Right Hand Analog
  * Head Left/Right Angle
  * Eye down switch
  * Eye up switch
  * Ear down switch
  * Ear up switch
  * IR Receive
  * Left Leg Analog
  * Right Leg Analog
  * Accelerometer
  * Microphone
  * Tilt switch
  * Touch Sensor Board (Serial, no clue)

27 Outputs
===

  * Left Palm LED
  * Right Palm LED
  * Chest LED
  * Left Leg Back
  * Left Leg Forward
  * Left Hand Down
  * Left Hand Up
  * Left Arm O? (Out?)
  * Left Arm C? (Center?)
  * Head Down
  * Head Up
  * Right Arm O?
  * Right Arm C?
  * Right Hand Down
  * Right Hand Up
  * Right Leg Back
  * Right Leg Forward
  * Eye Up
  * Eye Down
  * Head Left
  * Head Right
  * Ear Forward
  * Ear Back
  * IR Transmit
  * Left Eye LED
  * Right Eye LED
  * Speaker

Graphics
---

Motherboard
===

![RoboPanda Motherboard](https://gitlab.com/jdarling/robopanda/raw/master/RoboPanda_Pinout.png)

Motor Connector
===

![RoboPanda Motor Connector](https://gitlab.com/jdarling/robopanda/raw/master/robopanda-JP1-pinout.png)

Raspberry Pi 2 GPIO
---

![Raspberry Pi 2 GPIO](https://gitlab.com/jdarling/robopanda/raw/master/GPIO_Pi2.png)

![Raspberry Pi 2 Board with GPIO](https://gitlab.com/jdarling/robopanda/raw/master/RP2_Pinout.png)
